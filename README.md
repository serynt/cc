Spring MVC Web App getting rates from currencylayer.com

Overview
======
The app is based on/using:
- Spring Boot
- Spring MVC
- Spring Security
- Spring Cache
- JPA with embedded H2 DB
- JBehave - some simple BDD tests
- Maven is used as a build tool

Assumptions/restrictions
------
- free currencylayer.com account is used so source currencies are restricted to USD
- target currencies are resticted to few main currencies and can be easily modified/changed
- source currencies can be easily modified/changed
- at the moment L&F is not too important


How To Build
======
Prerequisities
- Ensure you have jdk 1.8 installed and pointed via JAVA_HOME
- Ensure you have maven 3.x installed

Configuration
------
- modify src/main/resources/application.properties file and provide (if you wish) other currencylayer.api.access.key

Run
------
Launch maven to run app with embedded Tomcat

mvn spring-boot:run
