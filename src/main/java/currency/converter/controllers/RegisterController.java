package currency.converter.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import currency.converter.dto.UserDTO;
import currency.converter.services.UserService;
import currency.converter.validators.RegisterValidator;

@Controller
public class RegisterController {
	
	private final UserService userService;
	private final RegisterValidator registerValidator;
	
	@Autowired
    public RegisterController(UserService userService, RegisterValidator registerValidator) {
        this.userService = userService;
        this.registerValidator = registerValidator;
    }
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(registerValidator);
    }
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView getRegisterPage() {
		return new ModelAndView("register", "dto", new UserDTO());
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String  registerUser(@Valid @ModelAttribute("dto") UserDTO dto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        try {
            userService.create(dto);
        } catch (DataIntegrityViolationException e) {
            bindingResult.reject("username.exists", "Username already exists");
            return "register";
        }
        return "redirect:/";
	}
	
}
