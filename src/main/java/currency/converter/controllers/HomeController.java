package currency.converter.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import currency.converter.dto.ExchangeDTO;
import currency.converter.services.ExchangeService;
import currency.converter.services.HistoryService;
import currency.converter.services.ExchangeService.Error;
import currency.converter.services.HistoryService.UserHistory;

@Controller
@Scope("request")
public class HomeController {
	@Autowired
	private ExchangeDTO exchangeDTO;

	@Autowired
	HistoryService historyService;
	
	@Autowired
	private ExchangeService exchangeService;
	
	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public ModelAndView getHomePage(Principal principal) {
		UserHistory history = historyService.getHistory(principal.getName());
		
		ModelAndView modelAndView = new ModelAndView("home", "dto", exchangeDTO);
		modelAndView.addObject("sourceCurriencies", exchangeService.getSourceCurrienciesAsList());
		modelAndView.addObject("targetCurriencies", exchangeService.getTargetCurrienciesAsList());
		modelAndView.addObject("history", history.asList());
		
		return modelAndView;
	}
	
	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.POST)
	public ModelAndView exchange(@Valid @ModelAttribute("dto") ExchangeDTO dto, BindingResult bindingResult, 
		       RedirectAttributes redirectAttributes, 
		       HttpServletRequest request, 
		       SessionStatus sessionStatus, Principal principal) {
		UserHistory history = historyService.getHistory(principal.getName());


		if (!bindingResult.hasErrors()) {
			try {
				Double rate;
				rate = exchangeService.exchange(dto.getDate(), dto.getSrc(), dto.getTarget());
				history.add(dto.getDate(), dto.getSrc(), dto.getTarget(), rate);
				
				exchangeDTO.setRate(rate);
				
			} catch (Error e) {
				bindingResult.reject("exchange.error", e.getMessage());
			} catch (Throwable t) {
				bindingResult.reject("general.error", t.getMessage());
			}
		}
		if (bindingResult.hasErrors()) {
			exchangeDTO.setRate(null);
			ModelAndView modelAndView = new ModelAndView("home");
			modelAndView.addObject("sourceCurriencies", exchangeService.getSourceCurrienciesAsList());
			modelAndView.addObject("targetCurriencies", exchangeService.getTargetCurrienciesAsList());
			modelAndView.addObject("history", history.asList());
			return modelAndView;
        }

		exchangeDTO.setDate(dto.getDate());
		exchangeDTO.setSrc(dto.getSrc());
		exchangeDTO.setTarget(dto.getTarget());
		
//		redirectAttributes.addFlashAttribute("message", "Success. The record was saved");
        return new ModelAndView("redirect:" + request.getRequestURI());
	}
}
