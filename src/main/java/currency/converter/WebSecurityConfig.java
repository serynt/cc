package currency.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import currency.converter.domain.Role;
import currency.converter.dto.UserDTO;
import currency.converter.services.CurrencyUserDetailsService;
import currency.converter.services.UserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CurrencyUserDetailsService currencyUserDetailsService;
	
	@Autowired
	private UserService userService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/register").permitAll()
			.anyRequest().authenticated();
		
		http.formLogin()
			.loginPage("/login")
			.permitAll();

		http.logout()
			.invalidateHttpSession(true)                                             
			.permitAll();
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		auth.userDetailsService(currencyUserDetailsService)
                 .passwordEncoder(encoder);
		
		if (!userService.userExists("user")) {
			UserDTO dto = new UserDTO();
			dto.setUsername("user");
			dto.setPassword("password");
			dto.setRole(Role.USER);
			
			userService.create(dto);
		}
         
    }
	
}
