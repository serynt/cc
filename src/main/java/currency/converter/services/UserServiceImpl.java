package currency.converter.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import currency.converter.domain.User;
import currency.converter.dto.UserDTO;
import currency.converter.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
	@Override
	public User getUserByUsername(String username) {
		return userRepository.findOneByUsername(username);
	}

	@Override
	public User create(UserDTO dto) {
		User user = new User();
		
		user.setUsername(dto.getUsername());
		user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
		user.setRole(dto.getRole());
		
		return userRepository.save(user);
	}

	@Override
	public boolean userExists(String username) {
		return userRepository.findOneByUsername(username) != null;
	}

}
