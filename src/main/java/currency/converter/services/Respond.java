package currency.converter.services;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Respond implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Error {
		private int code;
		private String info;
		
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getInfo() {
			return info;
		}
		public void setInfo(String info) {
			this.info = info;
		}
		
		@Override
		public String toString() {
			return "error.code: " + code + ", error.info: " + info;
		}
	}
	
	private boolean success;
	private SourceCurrency source;
	private Map<String, Double> quotes;
	private Date date;
	private Error error;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public SourceCurrency getSource() {
		return source;
	}
	public void setSource(SourceCurrency source) {
		this.source = source;
	}
	public Map<String, Double> getQuotes() {
		return quotes;
	}
	public void setQuotes(Map<String, Double> quotes) {
		this.quotes = quotes;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return "success: " + success + ", quotes: " + quotes + ", error: " + error;
	}
	
	public Double getRate(TargetCurrency target) {
		if (quotes != null && source != null) {
			return quotes.get("" + source.toString() + target.toString());
		}
		return null;
	}
}
