package currency.converter.services;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;
import currency.converter.services.ExchangeServiceImpl.Singleton;

@Service
@CacheConfig(cacheNames="respond")
public class CurrencylayerService {
	private static final String URL_LIVE = "http://apilayer.net/api/live";
	private static final String URL_HISTORICAL = "http://apilayer.net/api/historical";

	@Value("${currencylayer.api.access.key}")
	private String apiKey;

	@CacheEvict(key="#src.toString() + #target + #date")
	public void evict(LocalDate date, SourceCurrency src, TargetCurrency target) {
		
	}
	
	@CacheEvict(key="#src.toString() + #target")
	public void evict(SourceCurrency src, TargetCurrency target) {
		
	}

	
	@Cacheable(key="#src.toString() + #target + #date")
	public Respond getHistoricalRate(LocalDate date, SourceCurrency src, TargetCurrency target) {
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL_HISTORICAL)
				.queryParam("access_key", apiKey)
				.queryParam("date", date.toString())
				.queryParam("currencies", Singleton.getInstance().getTargetCurriencies())
				.queryParam("format", "1");
		
		Respond respond = restTemplate.getForObject(builder.toUriString(), Respond.class);
//		System.out.println(respond.toString());
		return respond;
	}
	
	@Cacheable(key="#src.toString() + #target")
	public Respond getLiveRate(SourceCurrency src, TargetCurrency target) {
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL_LIVE)
				.queryParam("access_key", apiKey)
				.queryParam("currencies", Singleton.getInstance().getTargetCurriencies())
				.queryParam("format", "1");
		
		Respond respond = restTemplate.getForObject(builder.toUriString(), Respond.class);
		System.out.println(respond.toString());
		return respond;
	}

}
