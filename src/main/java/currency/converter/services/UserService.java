package currency.converter.services;

import currency.converter.domain.User;
import currency.converter.dto.UserDTO;

public interface UserService {
	User getUserByUsername(String username);
	User create(UserDTO user);
	boolean userExists(String username);
}
