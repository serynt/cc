package currency.converter.services;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;

@Service
public class HistoryService {
	private static int HISTORY_LENGTH = 10;
	
	public class Item {
		private LocalDate date;
		private SourceCurrency sourceCurrency;
		private TargetCurrency targetCurrency;
		private double exchangeRate;
		
		public Item() {
			
		}
		
		public Item(LocalDate date, SourceCurrency sourceCurrency, TargetCurrency targetCurrency, double exchangeRate) {
			super();
			this.date = date;
			this.sourceCurrency = sourceCurrency;
			this.targetCurrency = targetCurrency;
			this.exchangeRate = exchangeRate;
		}
		public LocalDate getDate() {
			return date;
		}
		public void setDate(LocalDate date) {
			this.date = date;
		}
		public SourceCurrency getSourceCurrency() {
			return sourceCurrency;
		}
		public void setSourceCurrency(SourceCurrency sourceCurrency) {
			this.sourceCurrency = sourceCurrency;
		}
		public TargetCurrency getTargetCurrency() {
			return targetCurrency;
		}
		public void setTargetCurrency(TargetCurrency targetCurrency) {
			this.targetCurrency = targetCurrency;
		}
		public double getExchangeRate() {
			return exchangeRate;
		}
		public void setExchangeRate(double exchangeRate) {
			this.exchangeRate = exchangeRate;
		}
		
		@Override
		public String toString() {
			return (date != null ? date + " " : "") + sourceCurrency + targetCurrency + " " + exchangeRate;
		}
	}
	
	public class UserHistory {
		
		private Item[] items = new Item[HISTORY_LENGTH];
		
		private int idx = HISTORY_LENGTH;
		private int len;
		
		synchronized public void add(LocalDate date, SourceCurrency sourceCurrency, TargetCurrency targetCurrency, double exchangeRate) {
			idx = (HISTORY_LENGTH + idx - 1) % HISTORY_LENGTH;

			items[idx] = new Item(date, sourceCurrency, targetCurrency, exchangeRate);

			if (len < HISTORY_LENGTH) {
				len++;
			}
		}
		
		synchronized public List<Item> asList() {
			List<Item> list = new LinkedList<Item>();
			for (int i = 0; i < len; i++) {
				int tmpIdx = (idx + i) % HISTORY_LENGTH;
				
				list.add(items[tmpIdx]);
			}
			return list;
		}
		
		@Override
		public String toString() {
			return asList().toString();
		}
	}
	
	private ConcurrentHashMap<String, UserHistory> map = new ConcurrentHashMap<>();
	
	synchronized public UserHistory getHistory(String user) {
		UserHistory userHistory = map.get(user);
		if (userHistory == null) {
			userHistory = new UserHistory();
			map.put(user, userHistory);
		}
		
		return userHistory;
	}
}
