package currency.converter.services;

import java.util.List;

import org.joda.time.LocalDate;


public interface ExchangeService {
	public static class Error extends Exception {
		private static final long serialVersionUID = 1L;

		public Error(String msg) {
			super(msg);
		}
	}
	
	public static enum SourceCurrency {
		USD
	};
	
	public static enum TargetCurrency {
		EUR, USD, GBP, NZD, AUD, JPY, HUF
	};
	
    List<SourceCurrency> getSourceCurrienciesAsList();
    List<TargetCurrency> getTargetCurrienciesAsList();

	Double exchange(LocalDate date, SourceCurrency src, TargetCurrency target) throws Error;
}
