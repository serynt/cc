package currency.converter.services;

import java.util.Arrays;
import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames="respond")
public class ExchangeServiceImpl implements ExchangeService {
	
	public enum Singleton
	{
	    INSTANCE;

	    private final String targetCurriencies;
	    
	    private final List<SourceCurrency> sourceCurrienciesAsList;
	    private final List<TargetCurrency> targetCurrienciesAsList; 
	    
	    Singleton()
	    {
	    	StringBuffer buf = new StringBuffer();
	        for (TargetCurrency currency: TargetCurrency.values()) {
	        	if (buf.length() > 0) {
	        		buf.append(',');
	        	}
	        	buf.append(currency.name());
	        }
	        targetCurriencies = buf.toString();
	        
	        sourceCurrienciesAsList = Arrays.asList(SourceCurrency.values());
	        targetCurrienciesAsList = Arrays.asList(TargetCurrency.values());
	    }

	    public List<SourceCurrency> getSourceCurrienciesAsList() {
			return sourceCurrienciesAsList;
		}

		public List<TargetCurrency> getTargetCurrienciesAsList() {
			return targetCurrienciesAsList;
		}

		public static Singleton getInstance()
	    {
	        return INSTANCE;
	    }

	    public String getTargetCurriencies()
	    {
	        return targetCurriencies;
	    }
	}
	
	@Value("${currencylayer.api.access.key}")
	private String apiKey;

	@Autowired
	private CurrencylayerService currencylayerService;

	@Override
	public List<SourceCurrency> getSourceCurrienciesAsList() {
		return Singleton.getInstance().getSourceCurrienciesAsList();
	}

	@Override
	public List<TargetCurrency> getTargetCurrienciesAsList() {
		return Singleton.getInstance().getTargetCurrienciesAsList();
	}

	@Override
	public Double exchange(LocalDate date, SourceCurrency src, TargetCurrency target) throws Error {
//		System.out.println("exchange " + src + " -> " + target + " " + date);
	
		Double rate = null;
		
		Respond respond;

		if (date == null) {
			respond = currencylayerService.getLiveRate(src, target);
		} else {
			respond = currencylayerService.getHistoricalRate(date, src, target);
		}

		if (respond != null) {
			if (respond.getError() != null) {
				if (date == null) {
					currencylayerService.evict(src, target);
				} else {
					currencylayerService.evict(date, src, target);
				}
				throw new Error(respond.getError().toString());
			}
			
			rate = respond.getRate(target);
		}
		
		return rate;
	}

}
