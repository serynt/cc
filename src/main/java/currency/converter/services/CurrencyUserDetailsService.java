package currency.converter.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import currency.converter.domain.User;
import currency.converter.repositories.UserRepository;

@Service
public class CurrencyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findOneByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("No user found with username: " + username);
		}

		GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().toString());
		
		UserDetails userDetails = (UserDetails)new org.springframework.security.core.userdetails.User(user.getUsername(), 
				user.getPassword(), Arrays.asList(authority));
		
		return userDetails;
	}

}
