package currency.converter.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import currency.converter.domain.Address;
import currency.converter.domain.Role;

public class UserDTO extends Address {

	private static final long serialVersionUID = 1L;

	@NotEmpty
    private String username = "";

    @NotEmpty
    private String password = "";

    @NotEmpty
    private String confirm = "";

    @NotNull
    private Role role = Role.USER;
    
    @Email
    @NotEmpty
    private String email;

    @Past
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate birthdayDate;
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(LocalDate birthdayDate) {
		this.birthdayDate = birthdayDate;
	}
}
