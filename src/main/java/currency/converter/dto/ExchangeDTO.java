package currency.converter.dto;

import javax.validation.constraints.Past;

import org.joda.time.LocalDate;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;

@Component
@Scope(value="session")
public class ExchangeDTO {
	private SourceCurrency src = SourceCurrency.USD;
	private TargetCurrency target = TargetCurrency.EUR;
	
	@Past
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate date;
	
	private Double rate;
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public SourceCurrency getSrc() {
		return src;
	}
	
	public void setSrc(SourceCurrency src) {
		this.src = src;
	}
	
	public TargetCurrency getTarget() {
		return target;
	}
	
	public void setTarget(TargetCurrency target) {
		this.target = target;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	@Override
	public String toString() {
		return (date != null ? date + " " : "") + src + target + (rate != null ? " " + rate : "");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ExchangeDTO) {
			ExchangeDTO dto = (ExchangeDTO) obj;
			return toString().equals(dto.toString());
		}
		return false;
	}
}
