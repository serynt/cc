package currency.converter.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import currency.converter.dto.UserDTO;
import currency.converter.services.UserService;

@Component
public class RegisterValidator implements Validator {

	private final UserService userService;

    @Autowired
    public RegisterValidator(UserService userService) {
        this.userService = userService;
    }
    
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(UserDTO.class);
	}

	
	@Override
	public void validate(Object target, Errors errors) {
		UserDTO dto = (UserDTO)target;
		
		validateUsername(errors, dto);
		validatePasswordAndConfirm(errors, dto);
		

	}
	
	private void validateUsername(Errors errors, UserDTO dto) {
		if (userService.userExists(dto.getUsername())) {
			errors.rejectValue("username", "username.exists", "User with provided name already exists");
		}
	}
	
	private void validatePasswordAndConfirm(Errors errors, UserDTO dto) {
        if (!dto.getPassword().equals(dto.getConfirm())) {
            errors.rejectValue("confirm", "confirm.dontmatch", "Passwords do not match");
        }
    }
}
