package currency.converter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import currency.converter.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findOneByUsername(String username);
}
