package currency.converter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.password;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import currency.converter.domain.User;
import currency.converter.dto.ExchangeDTO;
import currency.converter.dto.UserDTO;
import currency.converter.repositories.UserRepository;
import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;
import currency.converter.services.UserService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IntegrationTestSession {
    @Autowired
    private WebApplicationContext webapp;

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    private MockMvc mockMvc;
	private MockHttpSession session;

    private UserDTO userDTO;
    private String user;
    private String password;
    private ExchangeDTO exchangeDTO;
    
	public void setUp() {
    	mockMvc = MockMvcBuilders.webAppContextSetup(webapp).apply(SecurityMockMvcConfigurers.springSecurity()).build();
	}

    public void createUser(String user, String password) {
    	userDTO = new UserDTO();
    	
    	userDTO.setUsername(user);
    	userDTO.setPassword(password);
    	
    	
    	userService.create(userDTO);
    	
	}
    
    public void provideRegistrationDetails(String username, String password, String confirmation, String email, String birthdayDate, String street, String zipCode, String city, String country) {
    	this.user = username;
    	
    	userDTO = new UserDTO();
    	userDTO.setUsername(username);
    	userDTO.setPassword(password);
    	userDTO.setConfirm(confirmation);
    	userDTO.setEmail(email);
    	userDTO.setBirthdayDate(new LocalDate(birthdayDate));
    	userDTO.setStreet(street);
    	userDTO.setZipCode(zipCode);
    	userDTO.setCity(city);
    	userDTO.setCountry(country);
    }

	public void login(String user, String password) throws Exception {
		this.user = user;
		this.password = password;
	}
	
	public void logout() throws Exception {
		mockMvc.perform( SecurityMockMvcRequestBuilders.logout() )
		.andExpect(unauthenticated());
	}
	
	public void iAmSuccessfullyLoggedIn() throws Exception {
		mockMvc.perform(formLogin("/login").user("username", user).password("password", password))
//		.andDo(print())
		.andExpect(authenticated())
		.andReturn();
	}

	public void iAmNotSuccessfullyLoggedIn() throws Exception {
		mockMvc.perform(formLogin("/login").user("username", user).password("password", password))
//		.andDo(print())
		.andExpect(unauthenticated())
		.andReturn();
		;	
		}

	public void registerUserWithNoErrors() throws Exception {
		mockMvc.perform(post("/register")
				.param("username", userDTO.getUsername())
				.param("password", userDTO.getPassword())
				.param("confirm", userDTO.getConfirm())
				.param("email", userDTO.getEmail())
				.param("birthdayDate", userDTO.getBirthdayDate().toString("yyyy-MM-dd"))
				.param("street", userDTO.getStreet())
				.param("zipCode", userDTO.getZipCode())
				.param("city", userDTO.getCity())
				.param("country", userDTO.getCountry())
				.with(csrf())
				)
//		.andDo(print())
		.andExpect(model().hasNoErrors())
		.andReturn();
		;	
	}

	public void registerUserWithSomeErrors() throws Exception {
		mockMvc.perform(post("/register")
				.param("username", userDTO.getUsername())
				.param("password", userDTO.getPassword())
				.param("confirm", userDTO.getConfirm())
				.param("email", userDTO.getEmail())
				.param("birthdayDate", userDTO.getBirthdayDate().toString("yyyy-MM-dd"))
				.param("street", userDTO.getStreet())
				.param("zipCode", userDTO.getZipCode())
				.param("city", userDTO.getCity())
				.param("country", userDTO.getCountry())
				.with(csrf())
				)
//		.andDo(print())
		.andExpect(model().hasErrors())
		.andReturn();
		;	
	}

	public void accountExistsByUsername() throws Exception {
		User found = userRepository.findOneByUsername(user);
		if (found == null) {
			throw new Exception("account does not exist");
		}
	}

	public void accountDoesntExistsByUsername() throws Exception {
		User found = userRepository.findOneByUsername(user);
		if (found != null) {
			throw new Exception("account exists");
		}
		
	}

	public void provideExchangeDetails(SourceCurrency src, TargetCurrency target, String date) {
		exchangeDTO = new ExchangeDTO();
		exchangeDTO.setSrc(src);
		exchangeDTO.setTarget(target);
		exchangeDTO.setDate(new LocalDate(date));
	}

	public void checkExchangeRate() throws Exception {
		session = new MockHttpSession();
		
		if (exchangeDTO.getDate() != null) {
			mockMvc.perform(post("/").with(user(userDTO.getUsername()).password(userDTO.getPassword()))
					.param("src", exchangeDTO.getSrc().toString())
					.param("target", exchangeDTO.getTarget().toString())
					.param("date", exchangeDTO.getDate().toString())
					.with(csrf())
					.session(session)
					)
//			.andDo(print())
			.andExpect(model().hasNoErrors())
			.andReturn();
			;	
		} else {
			mockMvc.perform(post("/").with(user(userDTO.getUsername()).password(userDTO.getPassword()))
					.param("src", exchangeDTO.getSrc().toString())
					.param("target", exchangeDTO.getTarget().toString())
					.with(csrf())
					.session(session)
					)
//			.andDo(print())
			.andExpect(model().hasNoErrors())
			.andReturn();
			;	
		}
	}

	public void checkMostRecentHistoryItem(Double rate) throws Exception {
		exchangeDTO.setRate(rate);
		
		mockMvc.perform(get("/").with(user(userDTO.getUsername()).password(userDTO.getPassword()))
				.with(csrf())
				.session(session)
				)
//		.andDo(print())
		.andExpect(model().attribute("dto", exchangeDTO))
		;
	}



}
