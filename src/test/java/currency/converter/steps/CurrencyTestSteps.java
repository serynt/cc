package currency.converter.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import currency.converter.IntegrationTestSession;
import currency.converter.services.ExchangeService.SourceCurrency;
import currency.converter.services.ExchangeService.TargetCurrency;

@Component
public class CurrencyTestSteps {
    
    @Autowired
    private ApplicationContext applicationContext;
    
    private IntegrationTestSession testSession;
    
    @Given("existing authorization credentials $user:$password")
    public void givenUserWithPassword(String user, String password) {
    	testSession = applicationContext.getBean(IntegrationTestSession.class);

    	testSession.setUp();
    	testSession.createUser(user, password);
    }
 
    @Given("logged in user using $user:$password")
    public void givenLoggedInUser(String user, String password) throws Exception {
    	testSession = applicationContext.getBean(IntegrationTestSession.class);

    	testSession.setUp();
    	testSession.createUser(user, password);
    	testSession.login(user, password);
    }

    @Given("registration data login: $username, password: $password, confirmation: $confirm, email: $email, birthday date: $birthdayDate and address $street $zipcode $city $country")
    public void givenRegistrationData(String username, String password, String confirm, String email, String birthdayDate, String street, String zipCode, String city, String country) {
    	testSession = applicationContext.getBean(IntegrationTestSession.class);

    	testSession.setUp();
    	testSession.provideRegistrationDetails(username, password, confirm, email, birthdayDate, street, zipCode, city, country);;
    	
    }
    
    @When("I provide $user:$password on login form")
    public void whenILogin(String user, String password) throws Exception {
    	testSession.login(user, password);
    }
    
    @When("I logout")
    public void whenILogout() throws Exception {
    	testSession.logout();
    }

    @When("I registered account")
    public void whenIRegisteredAccount() throws Exception {
    	testSession.registerUserWithNoErrors();
    }
    
    @When("I provide source currency $src and target currency $target and date $date")
    public void whenIProvide(SourceCurrency src, TargetCurrency target, String date) throws Exception {
    	testSession.provideExchangeDetails(src, target, date);
    	testSession.checkExchangeRate();
    }
    
    @When("I registered account (with errors)")
    public void whenIRegisteredAccountWithErrors() throws Exception {
    	testSession.registerUserWithSomeErrors();
    }

    @Then("I am successfully login")
    public void thenIAmAuthorized() throws Exception {
    	testSession.iAmSuccessfullyLoggedIn();
    }

    @Then("I am not successfully login")
    public void thenIAmNotAuthorized() throws Exception {
    	testSession.iAmNotSuccessfullyLoggedIn();
    }

    @Then("account exists")
    public void thenMyAccountIsCreated() throws Exception {
    	testSession.accountExistsByUsername();
    }

    @Then("account does not exists")
    public void thenMyAccountIsNotCreated() throws Exception {
    	testSession.accountDoesntExistsByUsername();
    }

    @Then("currency rate should be $rate")
    public void thenCurrencyRateShouldBe(Double rate) throws Exception {
    	testSession.checkMostRecentHistoryItem(rate);
    }
}
