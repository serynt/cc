Feature: register

Narrative:
As a user
I want to create an account
So that I can use portal

Scenario: Successful register
Given registration data login: new1, password: pass, confirmation: pass, email: fake@email.pl, birthday date: 1970-01-01 and address street 00-000 City Poland
When I registered account
Then account exists

Scenario: Not successful register (confirm password)
Given registration data login: new2, password: pass, confirmation: pass_other, email: fake@email.pl, birthday date: 1970-01-01 and address street 00-000 City Poland
When I registered account (with errors)
Then account does not exists


Scenario: Not successful register (email)
Given registration data login: new2, password: pass, confirmation: pass, email: fake, birthday date: 1970-01-01 and address street 00-000 City Poland
When I registered account (with errors)
Then account does not exists
