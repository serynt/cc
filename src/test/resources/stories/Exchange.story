Feature: currency exchange

Narrative:
As a registered user
I want to check current and historical currency rates
So that I can know when should I exchange currencies

Scenario: Check historical USDEUR currency rate
Given existing authorization credentials ex1:ex1
When I provide source currency USD and target currency EUR and date 2001-01-01
Then currency rate should be 1.066155

