Feature: login

Narrative:
As a registered user
I want to login to the portal
So that I can check currency exchange rates

Scenario: Successful login
Given existing authorization credentials test:test
When I provide test:test on login form
Then I am successfully login

Scenario: Not successful login
Given existing authorization credentials test1:test1
When I provide test1:test2 on login form
Then I am not successfully login

